package com.example.leonardo.turismocuritibav2;

/**
 * Created by leonardo on 23/04/17.
 */

public class Place {

    private int placeId;
    private int placeName;
    private int placeImageId;
    private int placeDescription;

    public Place(int placeId, int placeName, int placeImageId, int placeDescription) {
        this.placeId = placeId;
        this.placeName = placeName;
        this.placeImageId = placeImageId;
        this.placeDescription = placeDescription;
    }

    public int getPlaceDescription() {
        return placeDescription;
    }

    public int getPlaceImageId() {
        return placeImageId;
    }

    public int getPlaceName() {
        return placeName;
    }

    public int getPlaceId() {
        return placeId;
    }
}
