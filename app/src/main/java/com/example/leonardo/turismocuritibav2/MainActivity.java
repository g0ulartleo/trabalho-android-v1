package com.example.leonardo.turismocuritibav2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private boolean checkedStates[];
    private TextView totalPrice;
    private Button buttonSettings;
    private Button buttonBuy;
    private SharedPreferences.OnSharedPreferenceChangeListener spChanged;
    private SharedPreferences sharedPreferences;
    private double price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.loadValues();
        this.loadAdapter(this.populatePlaces());
    }



    private void loadValues(){
        this.spChanged = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                updatePrice();
            }
        };
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.totalPrice = (TextView) findViewById(R.id.textview_total);
        this.buttonSettings = (Button) findViewById(R.id.button_preferencias);
        this.buttonBuy = (Button) findViewById(R.id.button_comprar);
        this.checkedStates = new boolean[10];
        for (int i = 0; i < 10; i++) {
            this.checkedStates[i] = false;
        }
        this.buttonSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(MainActivity.this, PreferenceActivity.class);
                startActivity(it);
            }
        });
        this.buttonBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buy();
            }
        });
        this.updatePrice();
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this.spChanged);
    }

    private void buy(){
        String name = sharedPreferences.getString("opt_name","");
        String sending = sharedPreferences.getString("opt_ship","");
        int plus = Integer.parseInt(sharedPreferences.getString("opt_plus","0"));
        if(name.isEmpty()){
            Toast.makeText(
                    getApplicationContext(),
                    "Por favor, cadastre nome na aba de configuração.",
                    Toast.LENGTH_LONG
            ).show();
            return;
        }
        if(sending.isEmpty()){
            Toast.makeText(
                    getApplicationContext(),
                    "Por favor, cadastre o meio de compra na aba de configuração.",
                    Toast.LENGTH_LONG
            ).show();
            return;
        }
        String text = "Olá, meu nome é "+name+" e desejo conhecer os seguintes lugares: ";
        int a = 0;
        for(int i = 0; i < 10; i++){
            if(this.checkedStates[i]){
                a = i + 1;
                String identifier = "title_"+a;
                int res = getResources().getIdentifier(identifier,"string",getPackageName());
                text += getString(res) + ", ";
            }
        }
        if(a == 0){
            Toast.makeText(
                    getApplicationContext(),
                    "Por favor, selecione pelo menos um destino.",
                    Toast.LENGTH_LONG
            ).show();
            return;
        }
        text = text.substring(0, text.length() - 2) + ". ";
        if(plus > 0){
            text += "Tenho "+plus+" acompanhante(s). ";
        }
        if(sharedPreferences.getBoolean("opt_lunch",false)){
            text += "O passeio deve incluir almoço. ";
        }
        text += "Estou ciente do total de R$"+String.format("%.2f", this.price)+" para este passeio";
        if(sending.equalsIgnoreCase("whatsapp")){
            sendWhatsApp(text);
        }else if(sending.equalsIgnoreCase("email")){
            sendEmail(text);
        }else{
            sendSMS(text);
        }

    }

    public void sendWhatsApp(String text){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setType("text/plain");
        intent.setPackage("com.whatsapp");
        if(intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }
    }

    public void sendEmail(String text){
        if(text.length() > 0){
            String address = "luis.trevisan@up.edu.br";
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto: "+address));
            intent.putExtra(Intent.EXTRA_SUBJECT, "Compra - Turismo Curitiba");
            intent.putExtra(Intent.EXTRA_TEXT, text);
            if(intent.resolveActivity(getPackageManager()) != null){
                startActivity(intent);
            }
        }
    }

    public void sendSMS(String text){
        if(text.length() > 0){
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setType("*/*");
            intent.setData(Uri.parse("smsto:999147696"));
            intent.putExtra("sms_body", text);
            intent.putExtra(Intent.EXTRA_STREAM,"");
            if(intent.resolveActivity(getPackageManager()) != null){
                startActivity(intent);
            }
        }
    }

    private void updatePrice(){
        this.price = 0;
        for(int i = 0; i < 10; i++){
            if(this.checkedStates[i]){
                this.price += 15.0;
            }
        }
        if(sharedPreferences.getBoolean("opt_lunch",false)){
            this.price += 30.0;
        }
        int people = Integer.parseInt(sharedPreferences.getString("opt_plus", "0")) + 1;
        this.price *= people;

        this.totalPrice.setText("Total: \nR$"+String.format("%.2f", this.price));
    }

    private void loadAdapter(ArrayList<Place> places){
        final ArrayAdapter<Place> adapter = new MyPlaceAdapter(this, places);
        ListView listView = (ListView) findViewById(R.id.listview_itens);
        listView.setAdapter(adapter);
    }

    private ArrayList<Place> populatePlaces(){
        ArrayList<Place> listOfPlaces = new ArrayList<>();
        listOfPlaces.add(new Place(1,R.string.title_1,R.drawable.img_1,R.string.description_1));
        listOfPlaces.add(new Place(2,R.string.title_2,R.drawable.img_2,R.string.description_2));
        listOfPlaces.add(new Place(3,R.string.title_3,R.drawable.img_3,R.string.description_3));
        listOfPlaces.add(new Place(4,R.string.title_4,R.drawable.img_4,R.string.description_4));
        listOfPlaces.add(new Place(5,R.string.title_5,R.drawable.img_5,R.string.description_5));
        listOfPlaces.add(new Place(6,R.string.title_6,R.drawable.img_6,R.string.description_6));
        listOfPlaces.add(new Place(7,R.string.title_7,R.drawable.img_7,R.string.description_7));
        listOfPlaces.add(new Place(8,R.string.title_8,R.drawable.img_8,R.string.description_8));
        listOfPlaces.add(new Place(9,R.string.title_9,R.drawable.img_9,R.string.description_9));
        listOfPlaces.add(new Place(10,R.string.title_10,R.drawable.img_10,R.string.description_10));
        return listOfPlaces;
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        outState.putBooleanArray("CHECKSTATES", checkedStates);
        super.onSaveInstanceState(outState);
    }
    @Override
    public void onRestoreInstanceState(Bundle saveInstanceState) {
        checkedStates = saveInstanceState.getBooleanArray("CHECKSTATES");
        updatePrice();
    }

    private class MyPlaceAdapter extends ArrayAdapter<Place>{

        public MyPlaceAdapter(Context context, ArrayList<Place> places) {
            super(context, 0, places);
        }

        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View itemView = convertView;
            Place currentPlace = getItem(position);

            if(itemView == null){
                //itemView = getLayoutInflater().inflate(R.layout.list_row, parent, false);
                itemView = LayoutInflater.from(getContext()).inflate(R.layout.list_row, parent, false);
            }

            ImageView imageView = (ImageView) itemView.findViewById(R.id.item_imgview);
            imageView.setImageResource(currentPlace.getPlaceImageId());

            TextView titleView = (TextView) itemView.findViewById(R.id.item_textview_title);
            titleView.setText(currentPlace.getPlaceName());

            TextView descriptionView = (TextView) itemView.findViewById(R.id.item_textview_description);
            descriptionView.setText(currentPlace.getPlaceDescription());

            CheckBox checkBox = (CheckBox) itemView.findViewById(R.id.item_checkbox);
            checkBox.setChecked(checkedStates[position]);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean currentlyChecked = checkedStates[position];
                    checkedStates[position] = !currentlyChecked;
                    updatePrice();
                    notifyDataSetChanged();
                }
            });

            return itemView;
        }
    }

}
