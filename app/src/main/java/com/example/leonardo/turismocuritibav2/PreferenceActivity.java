package com.example.leonardo.turismocuritibav2;

import android.content.Intent;
import android.os.Bundle;

/**
 * Created by leonardo on 23/04/17.
 */

public class PreferenceActivity extends android.preference.PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //carrega o Layout de preferências, o PreferenceScreen
        addPreferencesFromResource(R.xml.preference);

        //cria um Intent de resultado e seta resultado OK
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
    }
}
